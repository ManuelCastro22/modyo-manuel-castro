/* IMPORT GENERAL */
import { win,
	doc,
	addEv,
	que,
	queAll,
    gId } from "../general/var.js"



export const menu = () => {
    const hamBtn = doc.queAll('.hamburgerBtn')

	hamBtn.length > 0 && [...hamBtn].map( el => {
		el.addEv('click', e => {
			e.preventDefault()
			e.currentTarget.classList.toggle('active')
			doc.que('.nav').classList.toggle('active')
		})
	})
}