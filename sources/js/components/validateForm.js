// IMPORTS
import { win,
    doc,
    addEv,
    que,
    queAll,
    gId } from "../general/var.js"


// VALIDATE FORM
export const validateForm = () => {

    const formEls = doc.queAll('.form')

    // Get all forms
    formEls.length > 0 && Array.from(formEls).map( formEl => {

        // VALID FORM
        formToSend(formEl)

    })

}

function formToSend(formEl){

    // Inputs blur
    const allInputs = getAllInputs(formEl)
    let formObject = allInputs.map( inputEl => {
            return {
                id: inputEl.id,
                value: inputEl.value,
                stateValidation: validInputs(inputEl, false)
            }
        } )

    Array.from(allInputs).forEach( inputEl => {
        const changedInput = inputEl.addEv(inputEl.type === 'select-one' ? 'change' : 'blur', async () => {
            const resultValidationInput = await validInputs(inputEl)
            formObject = formObject.map( formO => {
                if( formO.id === inputEl.id ){
                    return {
                        ...formO,
                        value: inputEl.value,
                        stateValidation: resultValidationInput
                    }
                } else {
                    return formO
                }
            } )
        })
    } )

    // Submit
    formEl.addEv('submit', e => validForm(e, formObject))

}


// GET ALL INPUTS
function getAllInputs(formEl) {
    return Array.from(formEl.elements).filter( inputEl => inputEl.dataset.valid )
}


// VALID INPUTS
function validInputs(element, showErrors = true){

    const value = element.value
    let res

    switch( element.dataset.valid ){
        case 'normal':
            res = normalValid(element, 'Enter a valid value', showErrors)
            break
        case 'email':
            // res = normalValid(element, 'Enter a valid value', showErrors)
            res = emailValid(element, 'Enter a valid email', showErrors)
            break
        default:
            res = false
            break
    }

    return res
}


// VALID
function normalValid(el, message, showErrors = true){
    let stateValidation

    if(el.value.trim() !== '') {
        removeError(el)
        stateValidation = true
    } else {
        showError(el, message)
        stateValidation = false
    }

    if( !showErrors ) {
        removeError(el)
    }

    return stateValidation
}

// EMAIL VALID
function emailValid(el, message, showErrors = true) {
    const emailRegex = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/
    let stateValidation

    if( el.value.match(emailRegex) ) {
        removeError(el)
        stateValidation = true
    } else {
        showError(el, message)
        stateValidation = false
    }

    if( !showErrors ) {
        removeError(el)
    }

    return stateValidation
}


// SHOW ERROR
function showError( element, message) {
    const elId = element.id,
        parentEl = element.closest('.form__box'),
        messageEl = parentEl.que('.form__message')


    doc.gId(elId).classList.add('error')
    messageEl.textContent = message
    messageEl.classList.remove('hide')
}


// REMOVE ERROR
function removeError( element ) {
    const elId = element.id,
        parentEl = element.closest('.form__box'),
        messageEl = parentEl.que('.form__message')


    doc.gId(elId).classList.remove('error')
    messageEl.textContent = ''
    messageEl.classList.add('hide')
}


// VALID FORM
function validForm(e, formObject){

    e.preventDefault()

    const hasError = formObject.some( itemFormObject => itemFormObject.stateValidation === false )

    if( hasError ){
        const toValidate = formObject.filter( itemFormObject => itemFormObject.stateValidation === false )
        Array.from(toValidate).map( inputEl => validInputs( doc.gId(inputEl.id) ) )
    } else {
        console.log(formObject)
    }

}