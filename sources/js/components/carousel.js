/* IMPORT GENERAL */
import { win,
	doc,
	addEv,
	que,
	queAll,
    gId } from "../general/var.js"
// IMPORT SWIPER
import Swiper, { Navigation, Pagination, Autoplay } from 'swiper'
import 'swiper/swiper-bundle.css'
// IMPORT SERVICES
import { Data } from '../services/Data.js'



// Configure Swiper
Swiper.use([ Navigation, Pagination, Autoplay ])


export const carousel = async element => {

    // CAROUSEL TESTIMONIALS
    const carouselTestimonials = doc.gId('carouselTestimonials')

    if( carouselTestimonials ) {
        const dataService = new Data(),
            limitData = 4,
            usersEndpoint = `https://jsonplaceholder.typicode.com/users?_limit=${limitData}`,
            postsEndpoint = `https://jsonplaceholder.typicode.com/posts?_limit=${limitData}`

        await showTestimonials(carouselTestimonials, dataService, usersEndpoint, postsEndpoint)

    }

    createCarousel(element)

}



// CREATE CAROUSEL
function createCarousel(element){

    const carousel = new Swiper(element, {
        speed: 1000,
        autoplay: {
            delay: 4000
        },
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination'
        }
    })

}


 // SHOW TESTIMONIALS
 async function showTestimonials(carouselTestimonials, dataService, usersEndpoint, postsEndpoint) {
    const allUsers = await getInfo(dataService, usersEndpoint),
        allPosts = await getInfo(dataService, postsEndpoint),
        unifiedData = allUsers.map( user => {
                const post = allPosts.filter( postItem => (user.id === postItem.id) )
                if( post.length > 0){
                    user = {
                        ...user,
                        photo: `person-${user.id}.jpg`,
                        post: {
                            title: post[0].title,
                            body: post[0].body
                        }
                    }
                }
                return user
            } )

    unifiedData.map( dataItem => {
        printTestimonials(carouselTestimonials, dataItem)
    } )
}


// GET INFO
function getInfo(dataService, endpoint){
    return dataService.getData(endpoint)
        .then( res => res )
        .catch( error => console.log(error) )
}


// PRINT TESTIMONIALS
function printTestimonials(carouselTestimonials, dataItem) {

    const { name, photo, post } = dataItem

    const templateEl = `
        <!-- CAROUSEL ITEM -->
        <article class="swiper-slide carousel__item">
            <picture class="carousel__item__user">
                <img src="img/people/${photo}" alt="" class="carousel__item__userImg">
            </picture>
            <cite class="carousel__item__cite">
                "${post.body}"
            </cite>
            <h3 class="carousel__item__author">
                ${name}
            </h3>
        </article>
        <!-- END CAROUSEL ITEM -->
    `

    carouselTestimonials.que('.carousel__wrapper').insertAdjacentHTML('beforeend', templateEl)
}