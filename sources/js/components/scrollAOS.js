/* IMPORT GENERAL */
import { win,
	doc,
	addEv,
	que,
	queAll,
    gId } from "../general/var.js"
// IMPORT AOS
import AOS from "aos"
import "aos/dist/aos.css"



export const scrollAOS = () => AOS.init()