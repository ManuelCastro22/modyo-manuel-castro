// IMPORTS
import {
	win,
	doc,
	addEv,
	que,
	queAll,
    gId } from '../general/var.js'



export const header = () => {

	const headerEl = doc.que('.header')


	headerEl && scrollHeader(headerEl)

	// Activate or deactivate scroll class in header
	headerEl && win.addEv('scroll', () => scrollHeader(headerEl) )

}


// TOGGLE SCROLL HEADER
function scrollHeader(headerEl) {
	headerEl.classList[ win.scrollY > 0 ? 'add' : 'remove' ]('scroll')
}