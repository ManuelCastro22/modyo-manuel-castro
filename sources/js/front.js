/* IMPORT GENERAL */
import {
	win,
	doc,
	addEv,
	que,
	queAll,
	gId
} from "./general/var.js"


/* IMPORTS */
import { openLoader } from './components/loader.js'
import { carousel } from './components/carousel'
import { scrollBar } from "./components/scrollBar.js"
import { validateForm } from "./components/validateForm.js"
import { menu } from "./components/menu.js"
import { header } from "./components/header.js"
import { scrollAOS } from "./components/scrollAOS.js"
// import { customPropertiesPolyfill } from './partials/customPropertiesPolyfill.js'
// import 'regenerator-runtime/runtime'


/* ON LOAD */
win.onload = () => doc.que('.loader').classList.add('hide')


/* LOAD COMPLETE */
doc.addEv('DOMContentLoaded', e => {


	// AOS
	scrollAOS()


	// OPEN LOADER
	openLoader()


	// MENU
	menu()


	// HEADER
	header()


	// CAROUSEL
	carousel('.carousel')


	// MODAL
	// modal()


	// SCROLL BAR
	scrollBar()


	// VALIDATE FORM
	validateForm()


})